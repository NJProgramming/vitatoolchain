#!/bin/sh
# build vita sdk.
# create target arm-vita-eabi

# check for VITASDK
if [ -z $VITASDK ]; then
	# no var set, no install location
	echo "VITASDK is not set! Please set before running this." >&2
	exit 1
fi

# make build directory
mkdir build

# move into directory
cd build || { echo "error moving into build dir." >&2; exit 1; }

# build toolchain
BUILD_SCRIPTS=(`ls ../scripts/*.sh | sort | grep -E [0-9]+-build-.*.sh`)

# execute scripts
for SCRIPT in ${BUILD_SCRIPTS[@]}; do "$SCRIPT" || { echo "$SCRIPT: Failed."; exit 1; } done

