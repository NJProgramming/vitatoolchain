#!/bin/sh
# ARM toolchain setup for clang/llvm

GCC_VERSION="gcc-4.7.1"

# first check for wget
command -v wget >/dev/null 2>&1 || { echo>&2 "wget is not installed. Install wget and retry."; exit 1; }

# chcek for gcc
command -v gcc >/dev/null 2>&1 || { echo>&2 "gcc is not installed. Install gcc and retry."; exit 1; }

# download gcc source
wget "ftp://ftp.gnu.org/pub/gnu/gcc/$GCC_VERSION/$GCC_VERSION.tar.bz2"

# extract gcc
tar -xvf "$GCC_VERSION.tar.bz2"

# change into gcc source dir
cd "$GCC_VERSION"

# download pre-req
"./contrib/download_prerequisites"

# exit
cd ..

# create build directory
mkdir gcc_build

# change to it
cd gcc_build

# configure
../$GCC_VERSION/configure --prefix="$VITASDK" --target="arm-vita-eabi" --enable-interwork --enable-languages="c,c++" --without-headers --with-newlib --with-gnu-as --with-gnu-ld

# build
make all-gcc
make install-gcc

# return
cd ..


