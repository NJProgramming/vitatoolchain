#!/bin/sh
# ARM toolchain setup for binutils
# by Davee 2012
# http://lolhax.org

# first check for wget
command -v wget >/dev/null 2>&1 || { echo>&2 "wget is not installed. Install wget and retry."; exit 1; }

# chcek for gcc
command -v gcc >/dev/null 2>&1 || { echo>&2 "gcc is not installed. Install gcc and retry."; exit 1; }

# download binutils source
wget "http://ftp.gnu.org/gnu/binutils/binutils-2.22.tar.bz2"

# extract binutils
tar -xvf "binutils-2.22.tar.bz2"

# create build directory
mkdir binutils_build

# change to it
cd binutils_build

# configure
../binutils-2.22/configure --prefix="$VITASDK" --target="arm-vita-eabi" --enable-interwork

# build
make
make install

# return
cd ..

