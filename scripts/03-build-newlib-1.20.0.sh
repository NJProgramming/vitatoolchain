#!/bin/sh
# ARM toolchain setup for newlib

# download newlib snapshot
wget ftp://sources.redhat.com/pub/newlib/newlib-1.20.0.tar.gz

# extract llvm
tar -xvf "newlib-1.20.0.tar.gz"

# create build directory
mkdir newlib_build

# change to it
cd newlib_build


# configure
../newlib-1.20.0/configure --prefix="$VITASDK" --target="arm-vita-eabi"

# build
make all
make install

# return
cd ..


